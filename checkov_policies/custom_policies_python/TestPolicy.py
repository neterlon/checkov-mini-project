from __future__ import annotations

from typing import Any

from checkov.terraform.checks.resource.base_resource_check import BaseResourceCheck
from checkov.common.models.enums import CheckResult, CheckCategories


class TestPolicy(BaseResourceCheck):
    def __init__(self) -> None:
        name = "Just a test policy"
        id = "TEST_PY_1"
        supported_resources = ("aws_vpc",)
        # CheckCategories are defined in models/enums.py
        categories = (CheckCategories.NETWORKING,)
        guideline = "There is not guideline for this policy"
        super().__init__(name=name, id=id, categories=categories, supported_resources=supported_resources, guideline=guideline)

    def scan_resource_conf(self, conf: dict[str, list[Any]]) -> CheckResult:
        if conf['cidr_block'][0] == "10.0.0.0/16":
            return CheckResult.PASSED
        else:
            return CheckResult.FAILED


check = TestPolicy()