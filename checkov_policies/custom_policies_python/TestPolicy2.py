from __future__ import annotations

from typing import Any

from checkov.terraform.checks.resource.base_resource_check import BaseResourceCheck
from checkov.common.models.enums import CheckResult, CheckCategories


class TestPolicy2(BaseResourceCheck):
    def __init__(self) -> None:
        name = "Just a test policy 2"
        id = "TEST_PY_2"
        supported_resources = ("aws_vpc",)
        # CheckCategories are defined in models/enums.py
        categories = (CheckCategories.NETWORKING,)
        guideline = "There is not guideline for this policy"
        super().__init__(name=name, id=id, categories=categories, supported_resources=supported_resources, guideline=guideline)

    def scan_resource_conf(self, conf: dict[str, list[Any]]) -> CheckResult:
        if conf['enable_dns_support'][0] == True:
            return CheckResult.PASSED
        else:
            return CheckResult.FAILED


check = TestPolicy2()